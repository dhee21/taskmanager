# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

1) User can create multiple lists with an editable title.

2) User can add multiple cards to lists.

(Cards will have a description, and a status.)

3) Option to delete a list.

4) Option to edit & delete card from a particular list.

5) Option to transfer a card from one list to another.

6) Data should persist after refreshing the page)

### How do I get set up? ###
1) have node and npm installed
2) create a project directory.
3) cd in that directory
4) npm install
5) npm start
6) it will create a server on http://localhost:3000
7) start using :)